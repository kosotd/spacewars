﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Game.Drawings.GameObjects.Concrete;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Drawings.Helpers;
using Kernel.Kernel.Helpers;

namespace Game.Kernel.Concrete
{
    public class MainGame : global::Kernel.Kernel.Abstract.Game
    {
        private IScreen screen;
        private IInput input;
        private List<Asteroid> asteroids;
        private List<Bullet> bullets; 
        private Spaceship spaceship;
        private Space space;
        private int lives;
        private int sumGenAsteroidTime, totalGenAsteroidTime;
        private Random random;
        private Animation crashAnimation;
        private bool[] pressedKeys;
        private bool gameOver;
        private int score;
        public MainGame(IScreen screen, IInput input)
        {
            this.screen = screen;
            this.input = input;
        }

        public override void Initialize()
        {
            score = 0;
            lives = 3;
            sumGenAsteroidTime = 0;
            totalGenAsteroidTime = 0;
            gameOver = false;
            pressedKeys = new bool[255];
            random = new Random();
            crashAnimation = new AlphaGradientAnimation(Color.Red, 255, 0, 20f);
            crashAnimation.IsDone = true;
            asteroids = new List<Asteroid>();
            bullets = new List<Bullet>();
            screen.Closing += (sender, args) => { StopGame(); };
            screen.LostFocus += (sender, args) =>
            {
                if (GameStarted())
                    Suspend();
            };
            screen.GotFocus += (sender, args) =>
            {
                if (GameSuspended())
                    Continue();
            };
            spaceship = new Spaceship(new PointF(0, 0), 16);
            spaceship.CreateSprites(SpriteManager.Instance);
            spaceship.CreateAnimations(AnimationManager.Instance);
            spaceship.position = new PointF(screen.GetSize().Width / 2f - spaceship.Bounds.Width, screen.GetSize().Height - spaceship.Bounds.Height * 2);
            space = new Space(screen.GetSize().Width, screen.GetSize().Height, 8);
            space.GenerateStars(100);
        }
        public override void LoadContent()
        {
        }

        public void GenerateAsteroid(GameTime gameTime)
        {
            sumGenAsteroidTime += gameTime.ElapsedGameTime.Milliseconds;
            if (sumGenAsteroidTime > totalGenAsteroidTime)
            {
                totalGenAsteroidTime = random.Next(500, 2000);
                sumGenAsteroidTime = 0;
                var asteroid = new Asteroid(new PointF(random.Next(0, screen.GetSize().Width), 0),
                    random.Next(50, 250), random.Next(8, 20));
                asteroid.CreateSprites(SpriteManager.Instance);
                asteroid.CreateAnimations(AnimationManager.Instance);
                asteroids.Add(asteroid);
            }
        }
        public override void Update(GameTime gameTime)
        {
            #if DEBUG
            updatesCount++;
            #endif

            if (gameOver)
            {
                space.Update(gameTime);
                if (input.IsKeyPressed(Key.Return))
                    NewGame();
                return;
            }

            space.Update(gameTime);
            spaceship.Update(gameTime);
            foreach (var bullet in bullets)
                bullet.Update(gameTime);
            for (int i = 0; i < asteroids.Count; ++i)
            {
                Asteroid asteroid = asteroids[i];
                asteroid.Update(gameTime);
                if (!asteroid.IsAlive && asteroid.IsDestroyFinished)
                {
                    asteroids.RemoveAt(i);
                    continue;
                }
                if (asteroid.IsAlive)
                    if (spaceship.Bounds.IntersectsWith(asteroid.Bounds))
                        if (CollisionHelper.SpritesIntersect(spaceship.GetSprite(), asteroid.GetSprite(),
                            spaceship.Bounds, asteroid.Bounds))
                        {
                            crashAnimation.StartNew();
                            crashAnimation.IsDone = false;
                            asteroid.IsAlive = false;
                            score -= asteroid.Score;
                            lives--;
                        }
                if (asteroid.IsAlive)
                    for (int j = 0; j < bullets.Count; ++j)
                    {
                        Bullet bullet = bullets[j];
                        if (bullet.position.Y < 0)
                            bullet.IsAlive = false;
                        if (bullet.Bounds.IntersectsWith(asteroid.Bounds))
                            if (CollisionHelper.SpritesIntersect(bullet.GetSprite(), asteroid.GetSprite(),
                                bullet.Bounds, asteroid.Bounds))
                            {
                                asteroid.SetDamage(bullet.Damage);
                                if (!asteroid.IsAlive)
                                    score += asteroid.Score;
                                bullet.IsAlive = false;
                            }
                        if (bullet.IsAlive == false)
                            bullets.RemoveAt(j);
                    }
                if (asteroid.IsAlive)
                    if (asteroid.position.Y > screen.GetSize().Height)
                    {
                        asteroid.IsAlive = false;
                        asteroid.IsDestroyFinished = true;
                    }
            }
            if (lives <= 0)
                gameOver = true;
            if (!crashAnimation.IsDone)
                crashAnimation.Update(gameTime);
            GenerateAsteroid(gameTime);
            ProcessInput(gameTime);
        }
        private void ProcessInput(GameTime gameTime)
        {
            if (input.IsKeyPressed(Key.Right))
                #if !DEBUG
                if (spaceship.position.X + spaceship.Bounds.Width < screen.GetSize().Width)
                #endif
                    spaceship.MoveRight(gameTime);
            if (input.IsKeyPressed(Key.Left))
                #if !DEBUG
                if (spaceship.position.X > 0)
                #endif
                spaceship.MoveLeft(gameTime);
            if (input.IsKeyPressed(Key.Space))
            {
                var bullet = spaceship.Fire();
                if (bullet != null)
                    bullets.Add(bullet);
            }
            if (input.IsKeyPressed(Key.Tab) && !pressedKeys[(int) Key.Tab])
            {
                pressedKeys[(int) Key.Tab] = true;
                spaceship.ChangeWeapons();
            }
            if (!input.IsKeyPressed(Key.Tab))
                pressedKeys[(int)Key.Tab] = false;
        }

        #if DEBUG
        private int updatesCount = 0;
        private int drawsCount = 0;
        #endif

        public override void Draw(GameTime gameTime)
        {
            #if DEBUG
            drawsCount++;
            #endif

            screen.Clear(Color.LightSkyBlue);
            if (gameOver)
            {
                space.Draw(screen);
                screen.DrawText("GAME OVER", new PointF(screen.GetSize().Width / 2f - 90, screen.GetSize().Height / 2f - 50), 
                    new Font(FontFamily.GenericSerif, 20), new SolidBrush(Color.Aquamarine));
                screen.DrawText("Score: " + score.ToString(), new PointF(screen.GetSize().Width / 2f - 60, 
                    screen.GetSize().Height / 2f - 10), new Font(FontFamily.GenericSerif, 20), new SolidBrush(Color.Aquamarine));
                screen.DrawText("Press Enter to start new game", new PointF(screen.GetSize().Width / 2f - 170,
                    screen.GetSize().Height / 2f + 30), new Font(FontFamily.GenericSerif, 20), new SolidBrush(Color.Aquamarine));
                screen.Render();
                return;
            }

            space.Draw(screen);
            spaceship.Draw(screen);
            foreach (var asteroid in asteroids)
                asteroid.Draw(screen);
            foreach (var bullet in bullets)
                bullet.Draw(screen);
            if (!crashAnimation.IsDone)
                crashAnimation.Draw(screen, PointF.Empty);

            screen.DrawText("Lives: " + lives.ToString(), new PointF(10, 10), new Font(FontFamily.GenericSerif, 10), new SolidBrush(Color.Aquamarine));
            screen.DrawText("Score: " + score.ToString(), new PointF(10, 30), new Font(FontFamily.GenericSerif, 10), new SolidBrush(Color.Aquamarine));
            screen.DrawText("Weapon: " + spaceship.GetWeaponName(), new PointF(10, 50), new Font(FontFamily.GenericSerif, 10), new SolidBrush(Color.Aquamarine));
            
            #if DEBUG
            screen.DrawText("updatesCount: " + updatesCount.ToString(), new PointF(10, 70), new Font(FontFamily.GenericSerif, 10), new SolidBrush(Color.Aquamarine));
            screen.DrawText("drawsCount: " + drawsCount.ToString(), new PointF(10, 90), new Font(FontFamily.GenericSerif, 10), new SolidBrush(Color.Aquamarine));
            #endif

            screen.Render();
        }
    }
}
