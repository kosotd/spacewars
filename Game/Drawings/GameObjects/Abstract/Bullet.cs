﻿using System.Drawing;
using Kernel.Kernel.Helpers;

namespace Game.Drawings.GameObjects.Abstract
{
    abstract class Bullet : AnimatedObject
    {
        private float speed;
        public float Damage;
        protected Bullet(PointF position, float speed, float damage) : base(position)
        {
            this.speed = speed;
            Damage = damage;
        }

        public override void Update(GameTime gameTime)
        {
            position.Y -= speed * 0.1f;
            base.Update(gameTime);
        }
    }
}
