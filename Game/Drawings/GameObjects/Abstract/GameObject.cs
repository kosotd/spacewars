﻿using System;
using System.Drawing;
using Game.Drawings.Concrete;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;
using RectangleF = System.Drawing.RectangleF;

namespace Game.Drawings.GameObjects.Abstract
{
    public abstract class GameObject : IDrawableObject
    {
        public PointF position;
        protected Sprite sprite;
        public bool IsAlive;
        public bool IsDestroyFinished;
        protected GameObject()
        {
            IsAlive = true;
        }
        protected GameObject(PointF position) : this()
        {
            this.position = position;
        }
        public abstract void CreateSprites(SpriteManager spriteManager);
        public virtual PointF Centroid
        {
            get
            {
                if (sprite == null) throw new NullReferenceException("sprite is null");
                return new PointF(position.X + sprite.Width/2.0f, position.Y + sprite.Height/2.0f);
            }
        }

        public void Draw(IScreen screen, PointF position)
        {
            if (sprite == null) throw new NullReferenceException("sprite is null");
            screen.Draw(sprite, position);
        }

        public virtual Sprite GetSprite()
        {
            return sprite;
        }
        public virtual RectangleF Bounds
        {
            get
            {
                if (sprite == null) throw new NullReferenceException("sprite is null");
                return new RectangleF((int) position.X, (int) position.Y, sprite.Width, sprite.Height);
            }
        }
        public abstract void Update(GameTime gameTime);
        public virtual void Draw(IScreen screen)
        {
            if (screen == null) throw new ArgumentNullException("screen");
            Draw(screen, position);
        }
    }
}
