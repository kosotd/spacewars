﻿using System;
using System.Drawing;
using Game.Drawings.Concrete;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;

namespace Game.Drawings.GameObjects.Abstract
{
    abstract class AnimatedObject : GameObject
    {
        protected AnimatedObject(PointF position) : base(position) { }
        public abstract Animation GetAnimation();
        public abstract void CreateAnimations(AnimationManager animationManager);
        public override void Draw(IScreen screen)
        {
            if (screen == null) throw new ArgumentNullException("screen");
            var animation = GetAnimation();
            if (animation == null) throw new NullReferenceException("Method GetAnimation returned null value");
            animation.Draw(screen, position);
        }

        public override RectangleF Bounds
        {
            get
            {
                var animation = GetAnimation();
                if (animation == null) throw new NullReferenceException("Method GetAnimation returned null value");
                return new RectangleF(position, animation.Bounds.Size);
            }
        }

        public override Sprite GetSprite()
        {
            var animation = GetAnimation();
            if (animation == null) throw new NullReferenceException("Method GetAnimation returned null value");
            return animation.GetSprite();
        }

        public override void Update(GameTime gameTime)
        {
            var animation = GetAnimation();
            if (animation == null) throw new NullReferenceException("Method GetAnimation returned null value");
            animation.Update(gameTime);
        }
    }
}
