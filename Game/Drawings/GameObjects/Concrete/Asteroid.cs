﻿using System.Drawing;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;

namespace Game.Drawings.GameObjects.Concrete
{
    class Asteroid : AnimatedObject
    {
        private Animation movingAnimation, destroyAnimation;
        private float movingSpeed, rotateSpeed;
        private Sprite movingSprite, destroySprite;
        private float hp = 100;
        public int Score = 13;
        public void SetDamage(float damage)
        {
            hp -= damage;
            if (hp <= 0)
                IsAlive = false;
        }
        public Asteroid(PointF position, float rotateSpeed = 50f, float movingSpeed = 8f) : base(position)
        {
            this.movingSpeed = movingSpeed;
            this.rotateSpeed = rotateSpeed;
        }
        public override Animation GetAnimation()
        {
            if (IsAlive)
                return movingAnimation;
            return destroyAnimation;
        }

        public override void CreateSprites(SpriteManager spriteManager)
        {
            spriteManager.CreateAsteroidSprites(out movingSprite, out destroySprite);
        }

        public override void CreateAnimations(AnimationManager animationManager)
        {
            animationManager.CreateAsteroidAnimations(movingSprite, destroySprite, rotateSpeed, 
                out movingAnimation, out destroyAnimation);
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsAlive)
            {
                if (destroyAnimation.IsDone)
                    IsDestroyFinished = true;
            }
            else
                position.Y += movingSpeed * 0.1f;
            base.Update(gameTime);
        }
    }
}
