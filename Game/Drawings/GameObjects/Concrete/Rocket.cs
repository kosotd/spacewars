﻿using System.Drawing;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Kernel.Drawings.Abstract;

namespace Game.Drawings.GameObjects.Concrete
{
    class Rocket : Bullet
    {
        private Animation animation;
        public Rocket(PointF position, float speed, float damage) : base(position, speed, damage) { }
        public override void CreateSprites(SpriteManager spriteManager)
        {
            spriteManager.CreateRocketSprite(out sprite);
        }
        public override void CreateAnimations(AnimationManager animationManager)
        {
            animationManager.CreateRocketAnimation(sprite, out animation);
            position.X -= sprite.Width/2f;
        }
        public override Animation GetAnimation()
        {
            return animation;
        }
    }
}
