﻿using System.Drawing;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Kernel.Drawings.Abstract;

namespace Game.Drawings.GameObjects.Concrete
{
    class Plasma : Bullet
    {
        private Animation animation;
        public Plasma(PointF position, float speed, float damage)
            : base(position, speed, damage)
        {
        }

        public override void CreateSprites(SpriteManager spriteManager)
        {
            spriteManager.CreatePlasmaSprite(out sprite);
        }

        public override void CreateAnimations(AnimationManager animationManager)
        {
            animationManager.CreatePlasmaAnimation(sprite, out animation);
            position.X -= sprite.Width / 2f;
        }

        public override Animation GetAnimation()
        {
            return animation;
        }
    }
}
