﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Kernel.Architecture.Abstract;
using Kernel.Kernel.Helpers;
using Color = System.Drawing.Color;

namespace Game.Drawings.GameObjects.Concrete
{
    class Space : GameObject
    {
        private int width, height;
        private float speed;
        List<Star> stars = new List<Star>(); 
        Random random = new Random();
        internal class Star
        {
            public float x, y;
            public int size;
            public System.Drawing.Color color;
            public void Draw(IScreen screen)
            {
                screen.DrawEllipse(new RectangleF(x - size / 2f, y - size / 2f, size, size), color, true);
            }
        }
        public Space(int width, int height, float speed)
        {
            this.speed = speed;
            this.width = width;
            this.height = height;
        }

        double GetRandomNumber(double minimum, double maximum)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
        public void GenerateStars(int count)
        {
            Random random = new Random();
            for (int i = 0; i < count; ++i)
            {
                float x = (float)GetRandomNumber(0, width);
                float y = (float)GetRandomNumber(0, height);
                int size = random.Next(1, 3);
                stars.Add(new Star
                {
                    x = x, 
                    y = y, 
                    size = size,
                    color = Color.GhostWhite
                });
            }
        }
        public override void Draw(IScreen screen)
        {
            screen.Clear(Color.Black);
            for (int i = 0; i < stars.Count; ++i)
                stars[i].Draw(screen);
        }

        public override void CreateSprites(SpriteManager spriteManager)
        {
            throw new NotImplementedException();
        }

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < stars.Count; ++i)
            {
                stars[i].y += speed * 0.1f;
                if (stars[i].y > height)
                {
                    stars[i].y = -1;
                    stars[i].x = (float)GetRandomNumber(0, width);
                }
            }
        }
    }
}
