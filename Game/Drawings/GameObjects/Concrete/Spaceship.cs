﻿using System.Drawing;
using Game.Drawings.Concrete;
using Game.Drawings.GameObjects.Abstract;
using Game.Drawings.GameObjects.Enums;
using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;

namespace Game.Drawings.GameObjects.Concrete
{
    class Spaceship : AnimatedObject
    {
        private float speed;
        private Animation moveUpAnimation, moveLeftAnimation, moveRightAnimation;
        private Animation currAnimation;
        private Sprite moveUpSprite, moveLeftSprite, moveRightSprite;
        private BulletType bulletType;
        private int currWeapon;
        private int cooldown;
        private int cooldownTime;
        public Spaceship(PointF position, float speed) : base(position)
        {
            this.speed = speed;
            bulletType = BulletType.Rocket;
            cooldown = 1000;
            cooldownTime = 1001;
            currWeapon = 0;
        }

        public void ChangeWeapons()
        {
            currWeapon ^= 1;
            bulletType = (BulletType) currWeapon;
            switch (bulletType)
            {
                case BulletType.Rocket:
                    cooldown = 1000;
                    break;
                case BulletType.Plasma:
                    cooldown = 500;
                    break;
            }
        }
        public Bullet Fire()
        {
            if (cooldownTime > cooldown)
            {
                cooldownTime = 0;
                Bullet result = null;
                switch (bulletType)
                {
                    case BulletType.Rocket:
                        result = new Rocket(new PointF(position.X + Bounds.Width/2, position.Y), 20, 50);
                        break;
                    case BulletType.Plasma:
                        result = new Plasma(new PointF(position.X + Bounds.Width / 2, position.Y), 40, 25);
                        break;
                }
                if (result != null)
                {
                    result.CreateSprites(SpriteManager.Instance);
                    result.CreateAnimations(AnimationManager.Instance);
                }
                return result;
            }
            return null;
        }
        public override Animation GetAnimation()
        {
            return currAnimation;
        }

        public string GetWeaponName()
        {
            switch (bulletType)
            {
                case BulletType.Rocket:
                    return "Rocket";
                case BulletType.Plasma:
                    return "Plasma";
            }
            return "";
        }
        public override void CreateSprites(SpriteManager spriteManager)
        {
            spriteManager.CreateSpaceshipSprites(out moveUpSprite, out moveLeftSprite, out moveRightSprite);
        }

        public override void CreateAnimations(AnimationManager animationManager)
        {
            animationManager.CreateSpaceshipAnimations(moveUpSprite, moveLeftSprite, moveRightSprite, 
                out moveUpAnimation, out moveLeftAnimation, out moveRightAnimation);
            currAnimation = moveUpAnimation;
        }

        public override void Update(GameTime gameTime)
        {
            cooldownTime += gameTime.ElapsedGameTime.Milliseconds;
            base.Update(gameTime);
            currAnimation = moveUpAnimation;
        }
        public void MoveLeft(GameTime gameTime)
        {
            currAnimation = moveLeftAnimation;
            position.X -= speed * 0.1f;
        }

        public void MoveRight(GameTime gameTime)
        {
            currAnimation = moveRightAnimation;
            position.X += speed * 0.1f;
        }
    }
}
