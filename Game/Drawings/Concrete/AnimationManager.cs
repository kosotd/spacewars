﻿using Kernel.Drawings.Abstract;
using Kernel.Drawings.Concrete;

namespace Game.Drawings.Concrete
{
    public class AnimationManager
    {
        private static AnimationManager _instance;
        private AnimationManager()
        {
        }
        public static AnimationManager Instance
        {
            get
            {
                _instance = _instance ?? new AnimationManager();
                return _instance;
            }
        }
        public void CreateSpaceshipAnimations(Sprite moveUpSprite, Sprite moveLeftSprite,
            Sprite moveRightSprite, out Animation moveUpAnimation, out Animation moveLeftAnimation,
            out Animation moveRightAnimation)
        {
            moveUpAnimation = new FrameAnimation(moveUpSprite, 39, 43, 3);
            moveLeftAnimation = new FrameAnimation(moveLeftSprite, 30, 43, 3);
            moveRightAnimation = new FrameAnimation(moveRightSprite, 29, 43, 3);
        }
        public void CreateAsteroidAnimations(Sprite movingSprite, Sprite destroySprite, float rotateSpeed, 
            out Animation movingAnimation, out Animation destroyAnimation)
        {

            movingAnimation = new RotateAnimation(movingSprite, rotateSpeed);
            destroyAnimation = new FrameAnimation(destroySprite, 53, 49, 6, 1, 1, 10f);
        }

        public void CreateRocketAnimation(Sprite rocketSprite, out Animation rocketAnimation)
        {
            rocketAnimation = new FrameAnimation(rocketSprite, 6, 15, 5, 1, 1, 50f);
        }
        public void CreatePlasmaAnimation(Sprite plasmaSprite, out Animation plasmaAnimation)
        {
            plasmaAnimation = new FrameAnimation(plasmaSprite, 3, 9, 2, 1, 1, 50f);
        }
    }
}
