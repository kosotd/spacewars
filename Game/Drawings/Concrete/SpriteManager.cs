﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Game.Content;
using Kernel.Drawings.Concrete;

namespace Game.Drawings.Concrete
{
    public class SpriteManager
    {
        private static SpriteManager _instance;
        private SpriteManager()
        {
        }
        public static SpriteManager Instance
        {
            get
            {
                _instance = _instance ?? new SpriteManager();
                return _instance;
            }
        }
        Random random = new Random();
        private List<Bitmap> asteroidBitmaps = new List<Bitmap>
        {
            Resources.asteroid_1,
            Resources.asteroid_2,
            Resources.asteroid_3,
            Resources.asteroid_4,
            Resources.asteroid_5,
            Resources.asteroid_6,
        };
        public void CreateSpaceshipSprites(out Sprite moveUpSprite, out Sprite moveLeftSprite,
            out Sprite moveRightSprite)
        {
            moveUpSprite = new Sprite(Resources.moveUp);
            moveLeftSprite = new Sprite(Resources.moveLeft);
            moveRightSprite = new Sprite(Resources.moveRight);
        }
        public void CreateAsteroidSprites(out Sprite movingSprite, out Sprite destroySprite)
        {
            movingSprite = new Sprite(asteroidBitmaps[random.Next(0, 5)]);
            destroySprite = new Sprite(Resources.asteroidDestroy);  
        }

        public void CreateRocketSprite(out Sprite rocketSprite)
        {
            rocketSprite = new Sprite(Resources.rocket);
        }
        public void CreatePlasmaSprite(out Sprite plasmaSprite)
        {
            plasmaSprite = new Sprite(Resources.plasma);
        }
    }
}
