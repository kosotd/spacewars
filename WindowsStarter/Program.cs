﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Kernel.Concrete;
using Kernel.Architecture.Abstract;
using Kernel.Architecture.Concrete;
using Kernel.Architecture.Enums;

namespace GameStarter
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var size = new Size(800, 480);
            var gameForm = new GameForm
            {
                ClientSize = size,
                StartPosition = FormStartPosition.CenterScreen,
                FormBorderStyle = FormBorderStyle.Fixed3D,
                MaximizeBox = false
            };
            IScreen screen = new WindowsScreen(gameForm.Handle, gameForm.ClientSize);
            IInput input = new KeyboardInput(gameForm.Handle);
            gameForm.Closing += (sender, args) =>
            {
                screen.SendMessage(ScreenMessage.SmClosing);
            };
            gameForm.LostFocus += (sender, args) => 
            { 
                screen.SendMessage(ScreenMessage.SmLostFocus);
            };
            gameForm.GotFocus += (sender, args) =>
            {
                screen.SendMessage(ScreenMessage.SmGotFocus);
            };
            gameForm.Show();
            (new MainGame(screen, input)).Run();
        }
    }
}
