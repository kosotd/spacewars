﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Kernel.Kernel.Enums;
using Kernel.Kernel.Helpers;

namespace Kernel.Kernel.Abstract
{
    public abstract class Game
    {
        private GameTime gameTime = new GameTime();
        TimeSpan TargetElapsedTime = TimeSpan.FromTicks(166667);
        private TimeSpan maxElapsedTime = TimeSpan.FromMilliseconds(500);
        private bool IsFixedTimeStep = true;
        private GameState gameState;
        private long previousTicks;
        private TimeSpan accumulatedElapsedTime;
        private int updateFrameLag;
        bool suppressDraw;
        private Stopwatch gameTimer;
        public void StopGame()
        {
            gameState = GameState.Stopped;
        }

        public bool GameStarted()
        {
            return gameState == GameState.Started;
        }
        public void NewGame()
        {
            gameState = GameState.NewGame;
        }

        public void Suspend()
        {
            gameState = GameState.Suspended;
        }

        public bool GameSuspended()
        {
            return gameState == GameState.Suspended;
        }
        public void Continue()
        {
            gameState = GameState.Started; 
        }
        public abstract void Initialize();
        public abstract void LoadContent();
        public abstract void Draw(GameTime gameTime);
        public abstract void Update(GameTime gameTime);
        public void Run()
        {
            Initialize();
            LoadContent();
            gameState = GameState.Started;
            gameTimer = Stopwatch.StartNew();
            while (gameState == GameState.Started || gameState == GameState.Suspended)
            {
                if (gameState == GameState.Started)
                    Tick();
                Application.DoEvents();
            }
            if (gameState == GameState.NewGame)
            {
                gameTime = new GameTime();
                IsFixedTimeStep = true;
                previousTicks = default(long);
                accumulatedElapsedTime = default(TimeSpan);
                updateFrameLag = default(int);
                suppressDraw = default(bool);
                Run();
            }
        }
        public void Tick()
        {
            RetryTick:
            var currentTicks = gameTimer.Elapsed.Ticks;
            accumulatedElapsedTime += TimeSpan.FromTicks(currentTicks - previousTicks);
            previousTicks = currentTicks;

            if (IsFixedTimeStep && accumulatedElapsedTime < TargetElapsedTime)
            {
                var sleepTime = (int)(TargetElapsedTime - accumulatedElapsedTime).TotalMilliseconds;
                Thread.Sleep(sleepTime);
                goto RetryTick;
            }

            if (accumulatedElapsedTime > maxElapsedTime)
                accumulatedElapsedTime = maxElapsedTime;

            if (IsFixedTimeStep)
            {
                gameTime.ElapsedGameTime = TargetElapsedTime;
                var stepCount = 0;
                while (accumulatedElapsedTime >= TargetElapsedTime)
                {
                    gameTime.TotalGameTime += TargetElapsedTime;
                    accumulatedElapsedTime -= TargetElapsedTime;
                    ++stepCount;

                    Update(gameTime);
                }
                updateFrameLag += Math.Max(0, stepCount - 1);

                if (gameTime.IsRunningSlowly)
                {
                    if (updateFrameLag == 0)
                        gameTime.IsRunningSlowly = false;
                }
                else if (updateFrameLag >= 5)
                {
                    gameTime.IsRunningSlowly = true;
                }
                if (stepCount == 1 && updateFrameLag > 0)
                    updateFrameLag--;
                gameTime.ElapsedGameTime = TimeSpan.FromTicks(TargetElapsedTime.Ticks * stepCount);
            }
            else
            {
                gameTime.ElapsedGameTime = accumulatedElapsedTime;
                gameTime.TotalGameTime += accumulatedElapsedTime;
                accumulatedElapsedTime = TimeSpan.Zero;

                Update(gameTime);
            }

            if (suppressDraw)
                suppressDraw = false;
            else
            {
                Draw(gameTime);
            }
        }
    }
}
