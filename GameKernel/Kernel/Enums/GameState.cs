﻿namespace Kernel.Kernel.Enums
{
    enum GameState
    {
        Started, Stopped, NewGame, Suspended
    }
}
