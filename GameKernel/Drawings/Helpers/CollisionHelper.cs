﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Kernel.Drawings.Concrete;

namespace Kernel.Drawings.Helpers
{
    public static class CollisionHelper
    {
        static void FixRect(Bitmap bitmap, ref RectangleF rect)
        {
            while (rect.Width >= bitmap.Width - rect.X)
                rect.Width -= 0.01f;
            while (rect.Height >= bitmap.Height - rect.Y)
                rect.Height -= 0.01f;
        }
        public static bool SpritesIntersect(Sprite oneSprite, Sprite otherSprite, RectangleF oneRect,
            RectangleF otherRect)
        {
            Bitmap one = oneSprite.GetBitmap();
            Bitmap other = otherSprite.GetBitmap();
            RectangleF intersect = oneRect;
            intersect.Intersect(otherRect);
            oneRect.Width = intersect.Width;
            oneRect.Height = intersect.Height;
            otherRect.Width = intersect.Width;
            otherRect.Height = intersect.Height;
            otherRect.X = intersect.X - otherRect.X;
            otherRect.Y = intersect.Y - otherRect.Y;
            oneRect.X = intersect.X - oneRect.X;
            oneRect.Y = intersect.Y - oneRect.Y;
            if (oneRect.Width < 1 || oneRect.Height < 1) return false;
            FixRect(one, ref oneRect);
            var onePart = one.Clone(oneRect, one.PixelFormat);
            try
            {
                if (otherRect.Width < 1 || otherRect.Height < 1) return false;
                FixRect(other, ref otherRect);
                var otherPart = other.Clone(otherRect, other.PixelFormat);
                try
                {
                    Rectangle rect = new Rectangle(0, 0, (int) intersect.Width, (int) intersect.Height);
                    var oneData = onePart.LockBits(rect, ImageLockMode.ReadOnly, onePart.PixelFormat);
                    try
                    {
                        byte[] oneRgb = new byte[oneData.Stride*oneData.Height];
                        Marshal.Copy(oneData.Scan0, oneRgb, 0, oneRgb.Length);
                        var otherData = otherPart.LockBits(rect, ImageLockMode.ReadOnly, otherPart.PixelFormat);
                        try
                        {
                            byte[] otherRgb = new byte[otherData.Stride*otherData.Height];
                            Marshal.Copy(otherData.Scan0, otherRgb, 0, otherRgb.Length);
                            int len = Math.Min(oneRgb.Length, otherRgb.Length);
                            for (int i = 0; i < len; ++i)
                                if (oneRgb[i] != 0 && otherRgb[i] != 0)
                                    return true;
                        }
                        finally
                        {
                            otherPart.UnlockBits(otherData);
                        }
                    }
                    finally
                    {
                        onePart.UnlockBits(oneData);
                    }
                }
                finally
                {
                    otherPart.Dispose();
                }
            }
            finally
            {
                onePart.Dispose();
            }
            return false;
        }
    }
}
