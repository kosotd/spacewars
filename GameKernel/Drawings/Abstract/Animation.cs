﻿using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;

namespace Kernel.Drawings.Abstract
{
    public abstract class Animation : IDrawableObject
    {
        private long time;
        private float animTime;
        public bool IsDone;
        protected Animation(float speed)
        {
            animTime = 1000/speed;
        }
        public virtual void Update(GameTime gameTime)
        {
            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time > animTime)
            {
                Animate();
                time = 0;
            }
        }
        public abstract void Animate();
        public abstract void StartNew();
        public abstract void Draw(IScreen screen, PointF position);
        public abstract Sprite GetSprite();
        public abstract RectangleF Bounds { get; }
    }
}
