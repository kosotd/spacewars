﻿using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Concrete;
using Kernel.Kernel.Helpers;

namespace Kernel.Drawings.Abstract
{
    public interface IDrawableObject
    {
        void Update(GameTime gameTime);
        void Draw(IScreen screen, PointF position);
        Sprite GetSprite();
        RectangleF Bounds { get; }
    }
}
