﻿using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;

namespace Kernel.Drawings.Concrete
{
    public sealed class FrameAnimation : Animation
    {
        private Sprite frameSet;
        private int framesCount, frameIndex;
        private int column, row;
        public int frameWidth, frameHeight;
        private RectangleF source;
        public FrameAnimation(Sprite frameSet, int frameWidth, int frameHeight, int framesCount, 
            int column = 1, int row = 1, float speed = 15f) : base(speed)
        {
            this.frameSet = frameSet;
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.framesCount = framesCount;
            this.column = column;
            this.row = row;
            frameIndex = 0;
            source = new RectangleF(frameWidth * (column - 1),
                (frameIndex + row - 1) * frameHeight, frameWidth, frameHeight);
        }

        public override void StartNew()
        {
            frameIndex = 0;
            source = new RectangleF(frameWidth * (column - 1),
                (frameIndex + row - 1) * frameHeight, frameWidth, frameHeight);
        }

        public override void Animate()
        {
            frameIndex++;
            if (frameIndex > framesCount - 1) 
            {
                frameIndex = 0;
                IsDone = true;
            }
            source = new RectangleF(frameWidth * (column - 1),
                (frameIndex + row - 1) * frameHeight, frameWidth, frameHeight);
        }

        public override RectangleF Bounds
        {
            get
            {
                return new RectangleF(0, 0, source.Width, source.Height);
            }
        }
        public override Sprite GetSprite()
        {
            var bitmap = frameSet.GetBitmap();
            return new Sprite(bitmap);
        }
        public override void Draw(IScreen screen, PointF position)
        {
            screen.Draw(frameSet, position, source);
        }
    }
}
