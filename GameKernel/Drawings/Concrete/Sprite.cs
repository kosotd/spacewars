﻿using System;
using System.Drawing;

namespace Kernel.Drawings.Concrete
{
    public class Sprite : IDisposable
    {
        private Bitmap data;
        public Sprite(Bitmap bitmap, Color transparentColor) : this(bitmap)
        {
            data.MakeTransparent(transparentColor);
        }
        public Sprite(Bitmap bitmap, int transparentPixelX, int transparentPixelY) : this(bitmap)
        {
            data.MakeTransparent(data.GetPixel(transparentPixelX, transparentPixelY));
        }
        public Sprite(Bitmap bitmap)
        {
            data = new Bitmap(bitmap);
        }
        public int Width
        {
            get { return data.Width; }
        }
        public int Height
        {
            get { return data.Height; }
        }
        public Size Size()
        {
            return data.Size;
        }
        public Bitmap GetBitmap()
        {
            return data;
        }
        public void Dispose()
        {
            data.Dispose();
        }
    }
}
