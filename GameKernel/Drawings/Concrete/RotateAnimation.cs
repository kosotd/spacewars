﻿using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;

namespace Kernel.Drawings.Concrete
{
    public class RotateAnimation : Animation
    {
        private Sprite sprite;
        private float currAngle;
        private Sprite newSprite;
        public RotateAnimation(Sprite sprite, float speed = 50f) : base(speed)
        {
            this.sprite = sprite;
            newSprite = RotateSprite(sprite, currAngle);
        }
        public override Sprite GetSprite()
        {
            return newSprite;
        }
        public override RectangleF Bounds
        {
            get
            {
                return new RectangleF(0, 0, newSprite.Width, newSprite.Height);
            }
        }

        public override void StartNew()
        {
            currAngle = 0;
            newSprite.Dispose();
            newSprite = RotateSprite(sprite, currAngle);
        }

        public override void Animate()
        {
            currAngle += 1;
            if (currAngle > 360)
            {
                currAngle = 0;
                IsDone = true;
            }
            newSprite.Dispose();
            newSprite = RotateSprite(sprite, currAngle);
        }
        public override void Draw(IScreen screen, PointF position)
        {
            screen.Draw(newSprite, position);
        }
        private Sprite RotateSprite(Sprite sprite, float angle)
        {
            Bitmap bmp = sprite.GetBitmap();
            Bitmap rotatedImage = new Bitmap(bmp.Width, bmp.Height);
            using (Graphics g = Graphics.FromImage(rotatedImage))
            {
                g.TranslateTransform(bmp.Width / 2f, bmp.Height / 2f); //set the rotation point as the center into the matrix
                g.RotateTransform(angle); //rotate
                g.TranslateTransform(-bmp.Width / 2f, -bmp.Height / 2f); //restore rotation point into the matrix
                g.DrawImage(bmp, new Point(0, 0)); //draw the image on the new bitmap
            }
            return new Sprite(rotatedImage);
        }
    }
}
