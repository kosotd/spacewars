﻿using System;
using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Drawings.Abstract;

namespace Kernel.Drawings.Concrete
{
    public class AlphaGradientAnimation : Animation
    {
        private Color color;
        private int startAlpha, finishAlpha, currAlpha, dx;

        public AlphaGradientAnimation(Color color, int startAlpha, int finishAlpha, float speed)
            : base(speed)
        {
            this.color = color;
            this.startAlpha = startAlpha;
            this.finishAlpha = finishAlpha;
            currAlpha = startAlpha;
            if (startAlpha < finishAlpha)
                dx = 17;
            else if (startAlpha > finishAlpha)
                dx = -17;
            else
                throw new ArgumentException("startAlpha can not be equal to finishAlpha");
        }

        public override void StartNew()
        {
            currAlpha = startAlpha;
        }

        public override void Animate()
        {
            currAlpha += dx;
            if (currAlpha.CompareTo(finishAlpha) == Math.Sign(dx))
            {
                currAlpha = startAlpha;
                IsDone = true;
            }
        }

        public override void Draw(IScreen screen, PointF position)
        {
            screen.FillColor(color, currAlpha);
        }

        public override Sprite GetSprite()
        {
            throw new NotImplementedException();
        }

        public override RectangleF Bounds { get { throw new NotImplementedException(); } }
    }
}
