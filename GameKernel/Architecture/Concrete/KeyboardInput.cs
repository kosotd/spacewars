using System;
using System.Windows.Input;
using Kernel.Architecture.Abstract;

namespace Kernel.Architecture.Concrete
{
    public class KeyboardInput : IInput
    {
        public KeyboardInput(IntPtr handle)
        {
        }
        public bool IsKeyPressed(Key key)
        {
            return Keyboard.IsKeyDown(key);
        }
    }
}
