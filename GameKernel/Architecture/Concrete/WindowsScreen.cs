﻿using System;
using System.Drawing;
using Kernel.Architecture.Abstract;
using Kernel.Architecture.Enums;
using Kernel.Drawings.Concrete;

namespace Kernel.Architecture.Concrete
{
    public class WindowsScreen : IScreen
    {
        private BufferedGraphics bg;
        private IntPtr handle;
        private Size size;
        public event ScreenDelegates.OnClosing Closing;
        public event ScreenDelegates.OnLostFocus LostFocus;
        public event ScreenDelegates.OnGotFocus GotFocus;

        public WindowsScreen(IntPtr handle, Size size)
        {
            this.size = size;
            this.handle = handle;
            BufferedGraphicsContext context = new BufferedGraphicsContext();
            context.MaximumBuffer = size;
            Graphics target = Graphics.FromHwnd(handle);
            bg = context.Allocate(target, new Rectangle(new Point(0, 0), size));
        }

        public IntPtr GetHandle()
        {
            return handle;
        }

        public void SendMessage(ScreenMessage screenMessage)
        {
            switch (screenMessage)
            {
                case ScreenMessage.SmClosing:
                    if (Closing != null) Closing(this, new EventArgs());
                    break;
                case ScreenMessage.SmLostFocus:
                    if (LostFocus != null) LostFocus(this, new EventArgs());
                    break;
                case ScreenMessage.SmGotFocus:
                    if (GotFocus != null) GotFocus(this, new EventArgs());
                    break;
            }
        }

        public void FillColor(Color color, int alpha = 255)
        {
            var brush = new SolidBrush(Color.FromArgb(alpha, color));
            bg.Graphics.FillRectangle(brush, 0, 0, GetSize().Width, GetSize().Height);
        }

        public void Clear(Color color)
        {
            bg.Graphics.Clear(color);
        }
        public void Draw(Sprite sprite, PointF position)
        {
            bg.Graphics.DrawImage(sprite.GetBitmap(), position);
        }

        public void DrawEllipse(RectangleF rect, Color color, bool filling = false)
        {
            Pen pen = new Pen(color);
            bg.Graphics.DrawEllipse(pen, rect);
            if (filling)
            {
                Brush brush = new SolidBrush(color);
                bg.Graphics.FillEllipse(brush, rect);
            }
        }

        public void Draw(Sprite sprite, PointF position, RectangleF rect)
        {
            bg.Graphics.DrawImage(sprite.GetBitmap(), new RectangleF(position, rect.Size), rect, 
                GraphicsUnit.Pixel);
        }

        public void DrawText(string text, PointF position, Font font, Brush brush)
        {
            bg.Graphics.DrawString(text, font, brush, position.X, position.Y);
        }

        public void Render()
        {
            bg.Render();
        }

        public Size GetSize()
        {
            return size;
        }
    }

   
}
