﻿using System.Windows.Input;

namespace Kernel.Architecture.Abstract
{
    public interface IInput
    {
        bool IsKeyPressed(Key key);
    }
}
