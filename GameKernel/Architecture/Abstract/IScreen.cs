﻿using System;
using System.Drawing;
using Kernel.Architecture.Enums;
using Kernel.Drawings.Concrete;

namespace Kernel.Architecture.Abstract
{
    public interface IScreen
    {
        event ScreenDelegates.OnClosing Closing;
        event ScreenDelegates.OnLostFocus LostFocus;
        event ScreenDelegates.OnGotFocus GotFocus;
        IntPtr GetHandle();
        void SendMessage(ScreenMessage screenMessage);
        void FillColor(Color color, int alpha = 255);
        void Clear(Color color);
        void Draw(Sprite sprite, PointF position);
        void DrawEllipse(RectangleF rect, Color color, bool filling = false);
        void Draw(Sprite sprite, PointF position, RectangleF rect);
        void DrawText(string text, PointF position, Font font, Brush brush);
        void Render();
        Size GetSize();
    }
}
