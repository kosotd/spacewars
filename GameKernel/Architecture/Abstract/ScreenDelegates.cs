﻿using System;

namespace Kernel.Architecture.Abstract
{
    public class ScreenDelegates
    {
        public delegate void OnClosing(object sender, EventArgs args);
        public delegate void OnLostFocus(object sender, EventArgs args);
        public delegate void OnGotFocus(object sender, EventArgs args);
    }
}
