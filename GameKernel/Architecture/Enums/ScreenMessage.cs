﻿namespace Kernel.Architecture.Enums
{
    public enum ScreenMessage
    {
        SmClosing, SmLostFocus, SmGotFocus
    }
}
